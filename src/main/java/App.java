import beans.Products;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.bind.*;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import static javax.xml.validation.SchemaFactory.newInstance;

public class App {

    private static final Source SOURCE_XML = new StreamSource("src/main/resources/products.xml");
    private static final Source SCHEMA_FILE = new StreamSource(new File("src/main/resources/products_schema.xsd"));
    private static final File OUTPUT_JSON = new File("src/main/resources/json");
    private static final File OUTPUT_XML = new File("src/main/resources/xml_from_json.xml");

    public static void main(String[] args) throws JAXBException, XMLStreamException, IOException {

        Products products;

        XMLInputFactory xmlInputFactory = XMLInputFactory.newFactory();
        XMLStreamReader xmlStreamReader = xmlInputFactory.createXMLStreamReader(SOURCE_XML);

        //Parsing XML
        JAXBContext jaxbContext = JAXBContext.newInstance(Products.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        JAXBElement<Products> jb = unmarshaller.unmarshal(xmlStreamReader, Products.class);
        products = jb.getValue();

        //Transforming POJO to JSON
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        String json = gson.toJson(products);
        FileOutputStream fileOutputStream = new FileOutputStream(OUTPUT_JSON);
        fileOutputStream.write(json.getBytes());

        //JSON to XML
        products = gson.fromJson(json, Products.class);
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.marshal(products, new FileOutputStream(OUTPUT_XML));

        System.out.println("---------Parsed XML---------\n" + products);
        //Validation
        validateXML();

        xmlStreamReader.close();
        fileOutputStream.close();
    }

    private static void validateXML() throws IOException {
        System.out.println("\n---------Validation---------");

        try {
            SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI)
                    .newSchema(SCHEMA_FILE)
                    .newValidator()
                    .validate(SOURCE_XML);
            System.out.println(SOURCE_XML.getSystemId() + " is valid");
        } catch (SAXException e) {
            System.out.println(SOURCE_XML.getSystemId() + " is NOT valid. Reason:" + e);
        }
    }
}
