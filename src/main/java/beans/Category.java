package beans;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.util.List;

public class Category {

    @XmlElement(name = "subcategory")
    private List<Subcategory> subcategory;
    @XmlAttribute
    private String categoryName;

    @Override
    public String toString() {
        return "\n" + categoryName.toUpperCase() + ":" + subcategory;
    }

}
