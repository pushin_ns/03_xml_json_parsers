package beans;

import service.DateAdapter;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Product {

    @XmlElement
    private String producer;
    @XmlElement
    private String model;
    @XmlElement
    @XmlSchemaType(name = "date")
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date date;
    @XmlElement
    private String color;
    @XmlElement
    private String priceAndNumber;

    private static String formatDate(Date date) {
        final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        return formatter.format(date);
    }

    @Override
    public String toString() {
        return "\n producer='" + producer + '\'' +
                ", model='" + model + '\'' +
                ", date=" + formatDate(date) +
                ", color='" + color + '\'' +
                ", priceAndNumber='" + priceAndNumber + '\'';
    }
}
