package beans;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
public class Products {

    @XmlElement(name = "category")
    private List<Category> categories;

    @Override
    public String toString() {
        return "Products:" + categories;
    }
}
