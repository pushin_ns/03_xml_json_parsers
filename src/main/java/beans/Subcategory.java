package beans;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.util.List;

class Subcategory {

    @XmlElement
    private List<Product> product;
    @XmlAttribute
    private String subcategoryName;

    @Override
    public String toString() {
        return "\n" + subcategoryName + ": \n" +
                product;
    }
}
