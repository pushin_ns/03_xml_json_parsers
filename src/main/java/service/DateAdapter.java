package service;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.text.SimpleDateFormat;
import java.util.Date;


public class DateAdapter extends XmlAdapter<String, Date> {

    private static final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public Date unmarshal(String v) throws Exception {
        synchronized (formatter) {
            return formatter.parse(v);
        }
    }

    @Override
    public String marshal(Date v) throws Exception {
        synchronized (formatter) {
            return formatter.format(v);
        }
    }
}
